# Bug with People Ops Engineering Projects
<!-- If it's a bug with GitLab as a product, please don't make a bug report here. -->

### What happened?
<!-- Please add all information that could help me investigate, give as many details as possible -->

### Related URLs (if applicable)
<!-- These can be AR issues, Slack messages (from bot outputs), ... -->
