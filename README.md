# People Operations Engineering

This issue tracker is dedicated to creating Workscope for People Operations Engineering.
Anything related to general Peopleops, should be opened in this [issue tracker](https://gitlab.com/gitlab-com/people-ops/General).

When you create a `New Issue` it will automatically default to our Engineering Request template. We encourage you to kindly use this format to ensure we have the information we need to prioritize and work on your request.
Please *do not* assign the issues to our People Operations Fullstack Engineer. You can assign all new issues to @vatalidis for triaging and review. 